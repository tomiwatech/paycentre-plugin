 package emv;

public enum CardHolderVerifyMethod {
	CHNone,
	CHOfflinePlainPin,
	CHOfflineEncipheredPin,
	CHOnlinePin,
	CHSignature
}
