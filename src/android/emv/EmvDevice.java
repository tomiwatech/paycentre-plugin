package emv;


import java.util.Date;

public interface EmvDevice {
	public String getSerialNumber();
	public String getRevision();
	public double getBatteryStatus();
	public boolean isTampered();
	public Date getSystemDate();
}
