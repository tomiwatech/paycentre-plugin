package emv;


public class EmvResponse {

	private String verboseFailureDescription;
	private String pan;
	
	private TransResult result;
	private CardHolderVerifyMethod verifyMethod;
	private boolean wasFallBack;
	private String tvr;
	private String iad;
	private String cryptogram;
	private String atc;
	private String amountAuthorized;
	private String amountOther;
	private long amountOtherLong;
	private String aip;
	private String unpredictableNo;
	private String transDate;
	private String dedicatedFileName;
	private String cryptogramInformationData;
	private String cvmResult;
	private String pinData;
	private String ksn;
	private String transactionType;
	private String terminalType;
	private String terminalCapabilities;
	private String terminalCountryCode;
	private long terminalCountryCodeLong;
	private String transactionCurrencyCode;
	private long transactionCurrencyCodeLong;
	private String transactionCategoryCode;
	private String tsi;
	private String appVersionName;
	private EmvCardData cardData;
	private boolean wasPinBypassed;
	private String panSequenceNo;
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	public TransResult getResult() {
		return result;
	}
	public void setResult(TransResult result) {
		this.result = result;
	}
	public CardHolderVerifyMethod getVerifyMethod() {
		return verifyMethod;
	}
	public void setVerifyMethod(CardHolderVerifyMethod verifyMethod) {
		this.verifyMethod = verifyMethod;
	}
	public boolean isWasFallBack() {
		return wasFallBack;
	}
	public void setWasFallBack(boolean wasFallBack) {
		this.wasFallBack = wasFallBack;
	}
	public String getTvr() {
		return tvr;
	}
	public void setTvr(String tvr) {
		this.tvr = tvr;
	}
	public String getIad() {
		return iad;
	}
	public void setIad(String iad) {
		this.iad = iad;
	}
	public String getCryptogram() {
		return cryptogram;
	}
	public void setCryptogram(String cryptogram) {
		this.cryptogram = cryptogram;
	}
	public String getAtc() {
		return atc;
	}
	public void setAtc(String atc) {
		this.atc = atc;
	}
	public String getAmountAuthorized() {
		return amountAuthorized;
	}
	public void setAmountAuthorized(String amountAuthorized) {
		this.amountAuthorized = amountAuthorized;
	}
	public String getAmountOther() {
		return amountOther;
	}
	public void setAmountOther(String amountOther) {
		this.amountOther = amountOther;
	}
	public String getAip() {
		return aip;
	}
	public void setAip(String aip) {
		this.aip = aip;
	}
	public String getUnpredictableNo() {
		return unpredictableNo;
	}
	public void setUnpredictableNo(String unpredictableNo) {
		this.unpredictableNo = unpredictableNo;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getDedicatedFileName() {
		return dedicatedFileName;
	}
	public void setDedicatedFileName(String dedicatedFileName) {
		this.dedicatedFileName = dedicatedFileName;
	}
	public String getCryptogramInformationData() {
		return cryptogramInformationData;
	}
	public void setCryptogramInformationData(String cryptogramInformationData) {
		this.cryptogramInformationData = cryptogramInformationData;
	}
	public String getCvmResult() {
		return cvmResult;
	}
	public void setCvmResult(String cvmResult) {
		this.cvmResult = cvmResult;
	}
	public String getPinData() {
		
		return pinData;
	}
	public void setPinData(String pinData) {
		this.pinData = pinData;
	}
	public String getKsn() {
		return ksn;
	}
	public void setKsn(String ksn) {
		this.ksn = ksn;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTerminalType() {
		return terminalType;
	}
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}
	public String getTerminalCapabilities() {
		return terminalCapabilities;
	}
	public void setTerminalCapabilities(String terminalCapabilities) {
		this.terminalCapabilities = terminalCapabilities;
	}
	public String getTerminalCountryCode() {
		return terminalCountryCode;
	}
	
	public long getTerminalCountryCodeLong() {
		return this.terminalCountryCodeLong;
	}
	public void setTerminalCountryCode(String terminalCountryCode) {
		this.terminalCountryCode = terminalCountryCode;
		this.terminalCountryCodeLong = Long.parseLong(terminalCountryCode);
	}
	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}
	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
		
		this.transactionCurrencyCodeLong = Long.parseLong(transactionCurrencyCode);
	}
	
	public long getTransactionCurrencyCodeLong() {
		return this.transactionCurrencyCodeLong;
	}
	
	public String getTransactionCategoryCode() {
		return transactionCategoryCode;
	}
	public void setTransactionCategoryCode(String transactionCategoryCode) {
		this.transactionCategoryCode = transactionCategoryCode;
	}
	public String getTsi() {
		return tsi;
	}
	public void setTsi(String tsi) {
		this.tsi = tsi;
	}
	public EmvCardData getCardData() {
		return cardData;
	}
	public void setCardData(EmvCardData cardData) {
		this.cardData = cardData;
	}
	public boolean isWasPinBypassed() {
		return wasPinBypassed;
	}
	public void setWasPinBypassed(boolean wasPinBypassed) {
		this.wasPinBypassed = wasPinBypassed;
	}
	public String getVerboseFailureDescription() {
		return verboseFailureDescription;
	}
	public void setVerboseFailureDescription(String verboseFailureDescription) {
		this.verboseFailureDescription = verboseFailureDescription;
	}
	public String getAppVersionName() {
		return appVersionName;
	}
	public void setAppVersionName(String appVersionName) {
		this.appVersionName = appVersionName;
	}
	public void setPanSequenceNo(String panSequenceNo) {
		this.panSequenceNo = panSequenceNo;
		
	}
	public String getPanSequenceNo() {
		return panSequenceNo;
		
	}
	
	
	

}
