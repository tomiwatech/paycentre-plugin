package emv.controllers;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

import controllers.PinpadHelper;
import controllers.PinpadManager;
import emv.EmvCardData;
import emv.EmvRequest;
import emv.EmvResponse;
import emv._0200Type;
import emv.utils.BerTlv;
import emv.utils.Tag;
import exceptions.TransactionAbortedException;
import exceptions.TransactionCanceledException;
import exceptions.TransactionDeniedException;
import exceptions.TransactionException;
import extras.ReferenceList;
import facade.PayPadManager;
import models.messaging.nibss.BalanceRequest;
import models.messaging.nibss.BalanceResponse;
import models.messaging.nibss.CashAdvanceRequest;
import models.messaging.nibss.CashAdvanceResponse;
import models.messaging.nibss.CashBackRequest;
import models.messaging.nibss.CashBackResponse;
import models.messaging.nibss.Globals;
import models.messaging.nibss._0200Request;
import models.messaging.nibss.NibssIsoProcessorVas;
import models.messaging.nibss.PurchaseRequest;
import models.messaging.nibss.PurchaseResponse;
import models.messaging.nibss.ReversalResponse;
import models.messaging.nibss.RePowerRequest;
import models.messaging.nibss.RePowerResponse;
import models.messaging.nibss.RefundRequest;
import models.messaging.nibss.RefundResponse;
import utils.HexUtil;

import com.datecs.pinpad.Pinpad;
import com.datecs.pinpad.PinpadException;
import com.datecs.pinpad.emv.EMVApplication;
import com.datecs.pinpad.emv.EMVCommonApplications;
import com.datecs.pinpad.emv.EMVStatusCodes;
import com.datecs.pinpad.emv.EMVTags;
import utils.CryptoUtil;
import org.json.JSONObject;
import org.json.JSONException;
import com.esl.lib.PaypadFacade;
import org.apache.cordova.*;

/**
 * Created by adeyemi on 3/14/16.
 */
public class EmvReversalProcessor {

    private static byte[] sessionKey, pinKey;

    String bdk;

    public EmvReversalProcessor(){

		bdk = ReferenceList.config.getString(Globals.BDKKey,"");

		String tsk = ReferenceList.config.getString(Globals.TSKVasKey, "none");

		// PaypadFacade.showStaticToast("tsk: "+tsk);

		if (!tsk.equals("none")) {
			sessionKey = HexUtil.hexStringToByteArray(tsk,' ');
		}

		String tpk = ReferenceList.config.getString(Globals.TPKVasKey, "none");

		// PaypadFacade.showStaticToast("tpk: "+tpk);

		if (!tpk.equals("none")) {
			pinKey = HexUtil.hexStringToByteArray(tpk,' ');
		}
    }

    public void doReversal(){

    	try{

    		// PaypadFacade.showStaticToast("starting reversal");

    		// PaypadFacade.showStaticToast("TSKVasKey : "+ReferenceList.config.getString(Globals.TSKVasKey, "none"));

	    	ReversalResponse response = NibssIsoProcessorVas.reversal(Globals._0200RequestVas,sessionKey);

	    	// PaypadFacade.showStaticToast("reversal ended "+response.getField39());

	    	response.returnMessage();

	    	JSONObject jsonObject = new JSONObject();
			jsonObject.put("operation","reversal");
			jsonObject.put("status", "done");
            // jsonObject.put("pan",Globals.cardNo);
            // jsonObject.put("cardholder",Globals.cardholder);
            // jsonObject.put("stan",Globals.stanVas);
            // jsonObject.put("rrn", Globals.rrnVas);
            // jsonObject.put("auth",Globals.authCode);
            // jsonObject.put("expiry",Globals.cardExpiry);
            jsonObject.put("responsecode",response.getField39());
            jsonObject.put("responsemessage",response.messages.get(response.getField39()));


			 // if(response.getField39() != null){
			PaypadFacade.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, jsonObject));
			 // }

			// PaypadFacade.showStaticToast("Reversal Done");

	    }catch(Exception ex){
	    	PaypadFacade.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, ""+ex.getMessage()));
	    }
    }
}
