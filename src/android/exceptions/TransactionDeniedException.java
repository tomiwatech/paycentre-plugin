package exceptions;

public class TransactionDeniedException extends TransactionException {
    private static final long serialVersionUID = 1L;
    
    public TransactionDeniedException(String reason) {
        super(reason);
    }
}
