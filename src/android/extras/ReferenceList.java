package extras;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class ReferenceList extends Activity {

	public static final String preference = "Configuration";
	public static final String FolderName = "FirstPad";
	public static SharedPreferences config;
	public static final String storageLocation = "slocation";
	public static int requestCode = 100;
	public static int max_input = 15;
	private String error = "Error in file";
	public static String password = "password";
	public static String amount = "0.00";
	public static String totalCashbackAmount;
	public static String cashbackAmount;
	public static String item = "";
	public static boolean progressbarstatus = false;
	public static boolean exit = false;
	public static String cashadvanceAmount;
	public static long otherAmount = 000000000000;
	public static String merchantTerminalID = "tid";
	public static String POSSerialNumber = "psn";
	public static String pinpadMACAddress = "pinpadMACAddress";
	public static String pinpadSerialNumber="pinpadSerialNumber";
	public static String activationCode = "activationCode";
	public static String isActivated = "isActivated";
	public static String isInit = "isInit";

	public static String userLocation = "";
	public static String TMK = "tmk";
	public static String TSK = "tsk";
	public static String TPK = "tpk";

	public static String TR31dupktKey = "TR31dupktKey";
	public static String TR31dataKey = "TR31dataKey";
	public static String TR31tmkKey = "TR31tmkKey";

	public static String stan = "stan";
	public static String rrn = "rrn";

	public static String acceptorID = "acceptorID";
	public static String acceptorName = "acceptorName";
	public static String merchantType = "merchantType";
	public static String terminal = "terminal";

	//activation details
	public static String accountNO = "accountNO";
	public static String terminalId = "terminalId";
	public static String businessName = "businessName";
	public static String merchantId = "merchantId";
	public static String phoneNo = "phoneNo";
	public static String bankName= "bankName";
	public static String nibssKeysDownloaded = "nibssKeysDownloaded";
	public static byte[] RAW_TMK = null;
	public static String merchantEmail = "merchantEmail";
	public static String businessAddress = "businessAddress";
	public static String bdk = "bdk";
	public static String isInitialized = "isInitialized";
	public static String username = "username";
	public static String oneTimeLogged = "oneTimeLogged";

	public static String ip = "ip";
	public static String port = "port";

	public ReferenceList() {
		// TODO Auto-generated constructor stub
	}

	public ReferenceList(String error) {
		// TODO Auto-generated constructor stub
		this.error = error;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

//	@Override
//	@Deprecated
//	protected Dialog onCreateDialog(int id) {
//		// TODO Auto-generated method stub
//
//		switch (id) {
//		case 0:
//			LayoutInflater inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//			View v = inflater.inflate(R.layout.alertview, null);
//			EditText eText = (EditText) v.findViewById(R.id.alertContent);
//			eText.setText(error);
//
//			return new AlertDialog.Builder(this)
//				.setIcon(R.drawable.launcher_icon)
//				.setTitle("A simple Dialog")
//				.setNeutralButton("OK", null)
//				.setView(v)
//				.create();
//
//		default:
//			return null;
//		}
//	}
}
