package models.messaging.nibss;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import utils.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/*
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
*/


public class GetMasterKeyResponse {
	

	private String field39;
	private String encryptedMasterKey;
	private byte[] clearMasterKey;
	
	public byte[] getClearMasterKey() {
		return clearMasterKey;
	}
	
	public void decryptMasterKey(byte[] transportKey){
		//Decrytion
		byte[] ctmkBytes = transportKey;
		byte[] encryptedBytes = HexUtil.hexStringToByteArray(encryptedMasterKey);		
		byte[] clearTMKBytes = CryptoUtil.decrypt3DESECB(ctmkBytes, encryptedBytes);		
		this.clearMasterKey = clearTMKBytes;
	}
	
	/**
	 * @param transportKey
	 * @throws java.security.NoSuchAlgorithmException
	 * @throws javax.crypto.NoSuchPaddingException
	 * @throws java.security.InvalidKeyException
	 * @throws java.security.spec.InvalidKeySpecException
	 * @throws javax.crypto.BadPaddingException
	 * @throws javax.crypto.IllegalBlockSizeException
	 */
	
	
	public void _decryptMasterKey(byte[] transportKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
		
		Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
		KeySpec spec = new DESedeKeySpec(transportKey);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
		SecretKey key  = factory.generateSecret(spec);
		
		cipher.init(Cipher.DECRYPT_MODE, key);
		
		this.clearMasterKey = cipher.doFinal(this.encryptedMasterKey.getBytes());
	}
	
	
	public String getEncryptedMasterKey() {
		return encryptedMasterKey;
	}

	public void setEncryptedMasterKey(String encryptedMasterKey) {
		this.encryptedMasterKey = encryptedMasterKey;
	}

	public String getField39() {
		return field39;
	}
	public void setField39(String field39) {
		this.field39 = field39;
	}
	
	

}
