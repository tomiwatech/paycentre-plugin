package models.messaging.nibss;

public class Globals {
	
	public static byte[] TMK = null; 
	public static byte[] TSK = null;
	public static byte[] TPK = null;
	public static long stan = 000005;
	public static long rrn = 000000000005;
	public static long stanVas = 0;
	public static long rrnVas = 0;
	public static String stanKey = "stan";
	public static String rrnKey = "rrn";

	public static String stanVasKey = "stanvas";
	public static String rrnVasKey = "rrnvas";
	public static String amount = "";
	public static String response = "";
	public static String bluetoothAddressKey = "bluetoothAddressKey";
	public static String bluetoothAddress = null;
	
	public static String terminalID = "";
	public static String terminalIDVas = "";
	public static String terminalIdKey = "terminalid";
	public static String terminalIdVasKey = "terminalidvas";
	public static String RRN = "";
	public static String responseCode = "";
	public static String tenderType = "";
	public static String entryMethod = "";
	public static String cardNo = "";
	public static String appResponse = "";
	public static String purchase = "";
	public static String amount1 = "";
	public static String cashback = "";
	public static String total = "";
	public static String AccountTypeFrom = "";
	public static String AccountTypeFromString = "";
	public static String reversalAmount = "";
	public static String cashadvance = "";
	public static String merchantTerminalID = null;  
	public static String merchantTerminalIDVas = null;
	public static String POSSerialNumber = null;
	public static String ipAddress = "196.6.103.73";
	public static int port = 5043;
	public static String downloadServerIP = "hsmu.paypad.com.ng";//"178.62.57.125";//"192.168.1.126";//"178.62.57.125";
	public static int downloadServerPort = 55528; //55513; //55505;
	public static String acceptorID = null;
	public static String acceptorIDKey = "acceptorID";
	public static String acceptorName = null;
	public static String acceptorNameKey = "acceptorName";
	public static String merchantType = null;
	public static String merchantTypeKey = "merchantType";

	// This variable are used for vas 
	public static String acceptorIDVas = null;
	public static String acceptorIDVasKey = "acceptorIDVas";
	public static String acceptorNameVas = null;
	public static String acceptorNameVasKey = "acceptorNameVas";
	public static String merchantTypeVas = null;
	public static String merchantTypeVasKey = "merchantTypeVas";

	public static String pass = "";
	public static String terminalCapabilities = "E040C8"; //"E0F8C8";
	public static boolean isPinpadConnected= false;
	public static boolean isPrinterConnected= false;

	public static String pinpadSerialNumber = "";
	public static boolean loadKeys = false;
	public static boolean autoConnect=false;
	
	/** new code**/
	public static String failedTransactionMessage = "";
	public static String regEmail = "";
	public static String regBizName = "";
	public static String regBizAddress = "";
	public static String regPhoneNo = "";
	public static String regPassword = "";
	public static String issuerName = "";
	public static String transactionType = "";
	public static String orderNo= "";
	public static String productsList = "";
	public static String totalOrderAmount = "";
	public static String receiverName= "";
	public static String receiverPhone= "";
	public static String receiverEmail= "";
	public static String receiverAddress= "";
	public static String receiverPix= "";
	public static boolean fromInit= false;
	public static byte[] pinKey = null;
	public static String transactionId = "";
	public static int numRetry = 0;
	
	public static String authData = null;
	public static String script71 = null;
	public static String script72 = null;
	public static String icc = null;
	public static String returnMessage = "";
	
	public static String crashedActivity = "";
	public static boolean pinpadKeysLoaded = true;
	public static String cardExpiry = null;
	public static String cardholder = null;
	public static String aid = null;
	public static String authCode = null;
	public static String transDate = null;
	public static double tender = 0;
	public static double change = 0;
	public static String srn = "";
	public static String plainSRN = "";
	public static String plainSRNKey = "plainsrn";

	public static String bdkKey = null;
	public static byte[] TR31dupktKey = null;
	public static byte[] TR31dataKey = null;
	public static byte[] TR31tmkKey = null;
	public static boolean connectedFromInitialization = false;
	public static String InitialMacAdd = "";
	public static PurchaseRequest purchaseRequest = null;
	public static CashBackRequest cashbackRequest = null;
	public static String purchaseDateStr = null;
	public static CashAdvanceRequest cashadvanceRequest = null;
	public static _0200Request _0200request = null;
	public static _0200Request _0200RequestVas = null;
	public static BalanceRequest balanceRequest =null;
	public static RefundRequest refundRequest = null;

	public static String preference = "preference";
	public static String TR31TmkKey = "tr31_tmk_key";
	public static String TPKKey = "tpk_key";
	public static String TSKKey = "tsk_key";
	public static String TR31DupktKey = "tr31_dukpt_key";
	public static String TR31DataKey = "tr31_dukpt_key";
	public static String DataKey = "data_key";
	public static String BDKKey = "bdk_key";
	public static String keyDownloaded = "keyDownloaded";
	public static String keyLoaded = "keyLoaded";
	public static String state = null;
	public static String selectedBank = null;


	// This section is also for others or VAS
	public static byte[] TR31tmkVasKey = null;
	public static String VAS_PRESENT_KEY = "vas_present";
	public static String TPKVasKey = "tpk_vas_key";
	public static String TSKVasKey = "tsk_vas_key";
	public static String TR31TmkVasKey = "tr31_tmk_key";
	public static boolean doVas = false;
	public static String printerAddressKey = "printer_address";
	public static String printerAddress = "";

}
