package models.messaging.nibss;

public class ReversalRequest {
	
	private String pan;
	private String processingCode;
	private String transactionAmount;
	private String transmissionDateTime;
	private String stan;
	private String localTransactionTime;
	private String localTransactionDate;
	private String expiryDate;
	private String merchantType;
	private String posEntryMode;
	private String cardSequenceNumber;
	private String posConditionCode;
	private String posPinCaptureCode;
	private String aquiringInstitutionIdCode;
	private String track2Data;
	private String retrievalReferenceNumber;
	private String serviceRestrictionCode;
	private String cardAcceptorTerminalId;
	private String cardAcceptorIdCode;
	private String cardAcceptorNameLocation;
	private String transactionCurrencyCode;
	private String pinData;
	private String additionalAmounts;
	private String messageReasonCode;
	private String transaportEchoData;
	private String payMentInformation;
	private String originalDataElements;
	private String replaceMentAmounts;
	private String posDataCode;
	private String seconndaryHashValue;
	private String expirationDate;
	private String iccData;
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getProcessingCode() {
		return processingCode;
	}
	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}
	public String getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}
	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public String getLocalTransactionTime() {
		return localTransactionTime;
	}
	public void setLocalTransactionTime(String localTransactionTime) {
		this.localTransactionTime = localTransactionTime;
	}
	public String getLocalTransactionDate() {
		return localTransactionDate;
	}
	public void setLocalTransactionDate(String localTransactionDate) {
		this.localTransactionDate = localTransactionDate;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public String getPosEntryMode() {
		return posEntryMode;
	}
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}
	public String getCardSequenceNumber() {
		return cardSequenceNumber;
	}
	public void setCardSequenceNumber(String cardSequenceNumber) {
		this.cardSequenceNumber = cardSequenceNumber;
	}
	public String getPosConditionCode() {
		return posConditionCode;
	}
	public void setPosConditionCode(String posConditionCode) {
		this.posConditionCode = posConditionCode;
	}
	public String getPosPinCaptureCode() {
		return posPinCaptureCode;
	}
	public void setPosPinCaptureCode(String posPinCaptureCode) {
		this.posPinCaptureCode = posPinCaptureCode;
	}
	public String getAquiringInstitutionIdCode() {
		return aquiringInstitutionIdCode;
	}
	public void setAquiringInstitutionIdCode(String aquiringInstitutionIdCode) {
		this.aquiringInstitutionIdCode = aquiringInstitutionIdCode;
	}
	public String getTrack2Data() {
		return track2Data;
	}
	public void setTrack2Data(String track2Data) {
		this.track2Data = track2Data;
	}
	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}
	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}
	public String getServiceRestrictionCode() {
		return serviceRestrictionCode;
	}
	public void setServiceRestrictionCode(String serviceRestrictionCode) {
		this.serviceRestrictionCode = serviceRestrictionCode;
	}
	public String getCardAcceptorTerminalId() {
		return cardAcceptorTerminalId;
	}
	public void setCardAcceptorTerminalId(String cardAcceptorTerminalId) {
		this.cardAcceptorTerminalId = cardAcceptorTerminalId;
	}
	public String getCardAcceptorIdCode() {
		return cardAcceptorIdCode;
	}
	public void setCardAcceptorIdCode(String cardAcceptorIdCode) {
		this.cardAcceptorIdCode = cardAcceptorIdCode;
	}
	public String getCardAcceptorNameLocation() {
		return cardAcceptorNameLocation;
	}
	public void setCardAcceptorNameLocation(String cardAcceptorNameLocation) {
		this.cardAcceptorNameLocation = cardAcceptorNameLocation;
	}
	public String getTransactionCurrencyCode() {
		return transactionCurrencyCode;
	}
	public void setTransactionCurrencyCode(String transactionCurrencyCode) {
		this.transactionCurrencyCode = transactionCurrencyCode;
	}
	public String getPinData() {
		return pinData;
	}
	public void setPinData(String pinData) {
		this.pinData = pinData;
	}
	public String getAdditionalAmounts() {
		return additionalAmounts;
	}
	public void setAdditionalAmounts(String additionalAmounts) {
		this.additionalAmounts = additionalAmounts;
	}
	public String getMessageReasonCode() {
		return messageReasonCode;
	}
	public void setMessageReasonCode(String messageReasonCode) {
		this.messageReasonCode = messageReasonCode;
	}
	public String getTransaportEchoData() {
		return transaportEchoData;
	}
	public void setTransaportEchoData(String transaportEchoData) {
		this.transaportEchoData = transaportEchoData;
	}
	public String getPayMentInformation() {
		return payMentInformation;
	}
	public void setPayMentInformation(String payMentInformation) {
		this.payMentInformation = payMentInformation;
	}
	public String getOriginalDataElements() {
		return originalDataElements;
	}
	public void setOriginalDataElements(String originalDataElements) {
		this.originalDataElements = originalDataElements;
	}
	public String getReplaceMentAmounts() {
		return replaceMentAmounts;
	}
	public void setReplaceMentAmounts(String replaceMentAmounts) {
		this.replaceMentAmounts = replaceMentAmounts;
	}
	public String getPosDataCode() {
		return posDataCode;
	}
	public void setPosDataCode(String posDataCode) {
		this.posDataCode = posDataCode;
	}
	public String getSeconndaryHashValue() {
		return seconndaryHashValue;
	}
	public void setSeconndaryHashValue(String seconndaryHashValue) {
		this.seconndaryHashValue = seconndaryHashValue;
	}
	
	public String getCardExpirationDate() {
		return expirationDate;
	}
	
	public void setCardExpirationDate(String cardExpirationDate) {
		this.expirationDate = cardExpirationDate;
		
	}

	public void setIccData(String iccData) {
		this.iccData = iccData;
		
	}
	
	public String getIccData(){
		return iccData;
	}
	

}
