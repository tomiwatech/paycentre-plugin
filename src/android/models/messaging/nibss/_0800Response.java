package models.messaging.nibss;

public class _0800Response {
	
	private String transmissionDateAndTime;
	private String systemTraceAuditNumber;
	private String timeLocalTransaction;
	private String dateLocalTransaction;
	private String cardAcceptorTerminalId;
	private String managementDataOne;
	private String managementDataTwo;
	private String messageHashValue;
	private String field39;
	
	
	public String getField39() {
		return field39;
	}
	public void setField39(String field39) {
		this.field39 = field39;
	}
	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}
	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}
	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}
	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}
	public String getTimeLocalTransaction() {
		return timeLocalTransaction;
	}
	public void setTimeLocalTransaction(String timeLocalTransaction) {
		this.timeLocalTransaction = timeLocalTransaction;
	}
	public String getDateLocalTransaction() {
		return dateLocalTransaction;
	}
	public void setDateLocalTransaction(String dateLocalTransaction) {
		this.dateLocalTransaction = dateLocalTransaction;
	}
	public String getCardAcceptorTerminalId() {
		return cardAcceptorTerminalId;
	}
	public void setCardAcceptorTerminalId(String cardAcceptorTerminalId) {
		this.cardAcceptorTerminalId = cardAcceptorTerminalId;
	}
	public String getManagementDataOne() {
		return managementDataOne;
	}
	public void setManagementDataOne(String managementDataOne) {
		this.managementDataOne = managementDataOne;
	}
	public String getManagementDataTwo() {
		return managementDataTwo;
	}
	public void setManagementDataTwo(String managementDataTwo) {
		this.managementDataTwo = managementDataTwo;
	}
	public String getMessageHashValue() {
		return messageHashValue;
	}
	public void setMessageHashValue(String messageHashValue) {
		this.messageHashValue = messageHashValue;
	}
}
