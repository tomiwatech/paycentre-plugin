package utils;

import java.util.List;
import java.util.Map;

/**
 * Created by adeyemi on 8/29/15.
 */
public class PrintModel {

    private String header;
    private List<String> subHeader;
    private String logoPath;
    private Map<String,String> body;
    private String poweredBy;
    private String footer;
    private String subFooter;
    private String footerImagePath;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<String> getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(List<String> subHeader) {
        this.subHeader = subHeader;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public Map<String, String> getBody() {
        return body;
    }

    public void setBody(Map<String, String> body) {
        this.body = body;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getFooterImagePath() {
        return footerImagePath;
    }

    public void setFooterImagePath(String footerImagePath) {
        this.footerImagePath = footerImagePath;
    }

    public String getPoweredBy() {
        return poweredBy;
    }

    public void setPoweredBy(String poweredBy) {
        this.poweredBy = poweredBy;
    }

    public String getSubFooter() {
        return subFooter;
    }

    public void setSubFooter(String subFooter) {
        this.subFooter = subFooter;
    }
}
