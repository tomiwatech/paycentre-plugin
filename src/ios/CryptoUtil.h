//
//  CryptoUtil.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/6/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSString+HexUtil.h"

@interface CryptoUtil : NSObject

-(NSString *) do3DESCBCDecryption : (NSString *) key
                             data : (NSString *) data
                               iv : (NSString*) iv;

-(NSString *) do3DESECBDecryption:(NSString *)key
                             data:(NSString *)data ;

-(NSString *) do3DESECBEncryption:(NSString *)key
                             data:(NSString *)data;
@end
