//
//  EMVResponse.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/8/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmvCardData.h"

@interface EMVResponse : NSObject

@property(getter=getPan, setter=setPan:, assign) NSString *pan;
@property(getter=getTVR, setter=setTVR:, assign) NSString *tvr;
@property(getter=getIAD, setter=setIAD:, assign) NSString *iad;
@property(getter=getATC, setter=setATC:, assign) NSString *atc;
@property(getter=getCryptogram, setter=setCryptogram:, assign) NSString *otherAmount;
@property(getter=getAmountAuthorised, setter=setAmountAuthorised:,assign) NSString * amountAuthorised;
@property(getter=getAmountOther, setter=setAmountOther:, assign)  NSString *amountOther;
@property(getter=getAIP, setter=setAIP:, assign) NSString *aip;
@property(getter=getUnpredictableNo, setter=setUnpredictableNo:, assign) NSString *unpredictableNo;
@property(getter=getTransDate, setter=setTransDate:, assign) NSString *transDate;
@property(getter=getDedicatedFileName, setter=setDedicatedFileName:, assign) NSString *dedicatedFileName;
@property(getter=getCryptogramInformationData, setter=setCryptogramInformationData:, assign) NSString *setCryptogramInformationData;
@property(getter=getCvmResult, setter=setCvmResult:, assign) NSString *cvmResult;
@property(getter=getPinData, setter=setPinData:, assign) NSString *pinData;
@property(getter=getTransactionType, setter=setTransactionType:, assign) NSString *transactionType;
@property(getter=getTerminalType, setter=setTerminalType:, assign) NSString *terminalType;
@property(getter=getTerminalCapabilities, setter=setTerminalCapabilities:, assign) NSString *terminalCapabilities;
@property(getter=getTerminalCountryCode, setter=setTerminalCountryCode:, assign) NSString *terminalCountryCode;
@property(getter=getTransactionCurrencyCode, setter=setTransactionCurrencyCode:, assign) NSString *transCurrencyCode;
@property(getter=getTransactionCategoryCode, setter=setTransactionCategoryCode:, assign) NSString *transCatCode;
@property(getter=getTSI, setter=setTSI:, assign) NSString *tsi;
@property(getter=getAppVersionName, setter=setAppVersionName:, assign) NSString *appVersionName;
@property(getter=getEmvCardData, setter=setEmvCardData:, assign) EmvCardData *cardData;
@property(getter=getPanSequenceNo, setter=setPanSequenceNo:, assign) NSString *panSequenceNo;

@end
