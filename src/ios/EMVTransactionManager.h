//
//  EMVTransactionManager.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/13/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTDevices.h"
#import "EMVRequest.h"
#import "ProgressViewController.h"
#import "dukpt.h"
#import "EMVTLV.h"
#import "EMVRequest.h"
#import "EMVResponse.h"
#import "EmvCardData.h"
#import "Global.h"
#import "PurchaseRequest.h"
#import "NibssIsoProcessor.h"
#import "PaypadFacade.h"

//extern EMVRequest *requestMain;
//extern EMVResponse *response;
//extern EmvCardData *cardData;

@interface EMVTransactionManager : NSObject  <UIAccelerometerDelegate>{
    
    ProgressViewController *progressViewController;
    DTDevices *dtdev;
    
//    EMVRequest *requestMain;
//    EMVResponse *response;
//    EmvCardData *cardData;
    
    NSString *track2Data;
    NSString *originalDataElements;
    NSString *replacementAmounts;
}

@property(strong,nonatomic) EMVResponse *response;
@property(strong,nonatomic) EMVRequest *requestMain;
@property(strong,nonatomic) EmvCardData *cardData;

-(id) initWithDTDevices : (DTDevices *) dtdevices;
- (void) doEMVTransaction : (EMVRequest *) request;


@end
