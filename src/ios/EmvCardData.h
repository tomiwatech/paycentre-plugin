//
//  EmvCardData.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/8/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmvCardData : NSObject

@property(getter=getAID, setter=setAID:, assign) NSString *aid;
@property(getter=getAppLabel, setter=setAppLabel:, assign) NSString *appLabel;
@property(getter=getPan, setter=setPan:, assign) NSData *pan;
@property(getter=getIin, setter=setIin:, assign) NSString *iin;
@property(getter=getCardHolderName, setter=setCardHolderName:, assign) NSString *cardholderName;
@property(getter=getCardSequenceNo, setter=setCardSequenceNo:, assign) NSString *sequenceNo;
@property(getter=getTrack2, setter=setTrack2:, assign) NSString *track2;
@property(getter=getTrack1, setter=setTrack1:, assign) NSString *track1;
@property(getter=getCardExpiryDate, setter=setCardExpiryDate:, assign) NSString *cardExpiryDate;


@end
