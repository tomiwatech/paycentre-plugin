//
//  Global.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/9/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const ACCOUNT_TYPE_FROM;
extern NSString *const STAN;
extern NSString *const STAN_VAS;
extern NSString *const RRN;
extern NSString *const RRN_VAS;
extern NSString *const CARD_HOLDER_NAME;
extern NSString *const POSENTRYMODE;
extern NSString *const POSCONDITIONCODE;
extern NSString *const POSPINCAPTURECODE;
extern NSString *const TRANSACTIONFEE;
extern NSString *const ACQUIRINGINSTIDCODE;
extern NSString *const POSDATACODE;
extern NSString *const TRANSPORT_DATA;
extern NSString *const TERMINAL_ID;
extern NSString *const TERMINAL_ID_VAS;
extern NSString *const AUTH_DATA;
extern NSString *const SCRIPT71;
extern NSString *const SCRIPT72;
extern NSString *const AUTH_CODE;
extern NSString *const RESPONSE_CODE;
extern NSString *const AMOUNT;
extern NSString *const INITIALIZATION_STATUS;
extern NSString *const PINPAD_SERIAL;
extern NSString *const PINPAD_CPU_SERIAL;
extern NSString *const VAS_PRESENT;


@interface Global : NSObject

@end
