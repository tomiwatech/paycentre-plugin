//
//  Global.m
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/9/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import "Global.h"

NSString *const ACCOUNT_TYPE_FROM = @"ACCOUNT_TYPE_FROM";
NSString *const STAN = @"STAN";
NSString *const STAN_VAS = @"STAN_VAS";
NSString *const RRN = @"RRN";
NSString *const RRN_VAS = @"RRN_VAS";
NSString *const CARD_HOLDER_NAME = @"CARD_HOLDER_NAME";
NSString *const POSENTRYMODE = @"051";
NSString *const POSCONDITIONCODE = @"00";
NSString *const POSPINCAPTURECODE = @"04";
NSString *const TRANSACTIONFEE = @"C00000000";
NSString *const ACQUIRINGINSTIDCODE = @"111129";
NSString *const POSDATACODE = @"511101512344101";
NSString *const TRANSPORT_DATA = @"000000000000";
NSString *const TERMINAL_ID = @"TERMINAL_ID";
NSString *const TERMINAL_ID_VAS = @"TERMINAL_ID_VAS";
NSString *const AUTH_DATA = @"AUTH_DATA";
NSString *const SCRIPT71 = @"SCRIPT71";
NSString *const SCRIPT72 = @"SCRIPT72";
NSString *const AUTH_CODE = @"AUTH_CODE";
NSString *const RESPONSE_CODE = @"RESPONSE_CODE";
NSString *const AMOUNT = @"AMOUNT";
NSString *const INITIALIZATION_STATUS = @"INITIALIZATION_STATUS";
NSString *const PINPAD_SERIAL= @"PINPAD_SERIAL";
NSString *const PINPAD_CPU_SERIAL= @"PINPAD_CPU_SERIAL";
NSString *const VAS_PRESENT = @"VAS_PRESENT";


@implementation Global

@end
