//
//  NSMutableData+NSMutableDataWithConversion.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 1/4/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableData (NSMutableDataWithConversion)
- (NSString *)hexadecimalString;
- (NSString *)hexadecimalCharacters;
@end
