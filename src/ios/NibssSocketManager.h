//
//  NibssSocketManager.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/11/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NibssSocketManager : NSObject<NSStreamDelegate>

-(void) connectToNibss;
-(void) sendMessageToNibss : (NSMutableData *) data ;
-(void) readMessageFromNibss ;

@end
