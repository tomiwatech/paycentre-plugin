
// This is the PaypadFacade that will be used in interfacing with the device
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Cordova/CDV.h>
#import "DTDevices.h"
#import "PinpadKeysResponse.pb.h"
#import "PinpadKeysRequest.pb.h"
#import "PaypadKeyManager.h"
#import <netdb.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Global.h"
#import "PinpadHelper.h"
#import <MessageUI/MessageUI.h>


id <CDVCommandDelegate> commandD;

@interface PaypadFacade : CDVPlugin <UIAccelerometerDelegate,NSStreamDelegate,MFMessageComposeViewControllerDelegate>{

	DTDevices *dtdev;
    NSString *serial;
}

// This is for downloading keys from the HSM
@property (nonatomic,strong) PinpadKeysRequest* pinkeyRequest;
@property (nonatomic,strong) PinpadKeysResponse* pinkeyResponse;

-(void) initialization : (CDVInvokedUrlCommand*)command ;
-(void) connection : (CDVInvokedUrlCommand*)command ;
-(void) payment : (CDVInvokedUrlCommand*)command ;
-(void) vas : (CDVInvokedUrlCommand*)command ;
-(void) network : (CDVInvokedUrlCommand*)command ;
-(void) startscan : (CDVInvokedUrlCommand*)command ;
-(void) stopscan : (CDVInvokedUrlCommand*)command ;
-(void) print : (CDVInvokedUrlCommand*)command ;
-(void) connectprinter : (CDVInvokedUrlCommand*)command ;
-(void) sms : (CDVInvokedUrlCommand*)command ;

-(void)connectToServer:(NSString*) ip
                  port:(NSUInteger)port;
                  
-(NSString*)dataToHexstring:(NSData*)data;
-(NSData*)hexStringToData:(NSString*)hexString;
-(BOOL)checkInternet;
-(void)makePayment:(NSString *)amount accountType:(NSString*)accountType doVas:(BOOL) doVas;
+(void)sendNotification:(NSDictionary *) dict wait:(BOOL) wait error:(BOOL) error;

@end