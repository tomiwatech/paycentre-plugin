//
//  PurchaseResponse.m
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/16/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import "PurchaseResponse.h"



@implementation PurchaseResponse

+(NSString *)getResponseString:(NSString *)value{
    
    NSDictionary *dict = @{
                           @"00": @"TRANSACTION \nSUCCESSFUL",
                           @"01": @"REFER TO \nCARD ISSUER",
                           @"02": @"REFER TO \nCARD ISSUER",
                           @"03": @"INVALID \nMERCHANT",
                           @"04": @"PICK-UP \nCARD",
                           @"05": @"DO NOT \nHONOUR",
                           @"06": @"ERROR",
                           @"07": @"PICK-UP \nCARD",
                           @"08": @"HONOUR \nWITH ID",
                           @"09": @"REQUEST \nIN PROGRESS",
                           @"10": @"APPROVED, \nPARTIAL",
                           @"11": @"APPROVED, \nVIP",
                           @"12": @"INVALID \nTRANSACTION",
                           @"13": @"INVALID \nAMOUNT",
                           @"14": @"INVALID \nCARD NUMBER",
                           @"15": @"NO SUCH \nISSUER",
                           @"16": @"APPROVED \nUPDATE TRACK3",
                           @"17": @"CUSTOMER \nCANCELLATION",
                           @"18": @"CUSTOMER \nDISPUTE",
                           @"19": @"RE-ENTER \nTRANSACTION",
                           @"20": @"INVALID \nRESPONSE",
                           @"21": @"NO ACTION \nTAKEN",
                           @"22": @"SUSPECTED \nMALFUNCTION",
                           @"23": @"UNACCEPTABLE \nFEE FOR TRANSACTION",
                           @"24": @"FILE UPDATE \nNOT SUPPORTED",
                           @"25": @"UNABLE TO \nLOCATE RECORD",
                           @"26": @"DUPLICATE \nRECORD",
                           @"27": @"FILE UPDATE;\n EDIT ERROR",
                           @"28": @"FILE UPDATE;\n FILE LOCKED",
                           @"29": @"FILE \nUPDATE FAILED",
                           @"30": @"FORMAT \nERROR",
                           @"31": @"BANK NOT \nSUPPORTED",
                           @"32": @"COMPLETED \nPARTIALLY",
                           @"33": @"EXPIRED CARD;\n PICK-UP",
                           @"34": @"SUSPECTED \nFRAUD; PICK-UP",
                           @"35": @"CONTACT \nACQUIRER; PICK-UP",
                           @"36": @"RESTRICTED \nCARD; PICK-UP",
                           @"37": @"CALL ACQUIRER \nSECURITY; PICK-UP",
                           @"38": @"PIN TRIES \nEXCEEDED; PICK-UP",
                           @"39": @"NO CREDIT \nACCOUNT",
                           @"40": @"FUNCTION NOT \nSUPPORTED",
                           @"41": @"LOST CARD",
                           @"42": @"NO UNIVERSAL \nACCOUNT",
                           @"43": @"STOLEN CARD",
                           @"44": @"NO INVESTMENT \nACCOUNT",
                           @"51": @"INSUFFICIENT \nFUND",
                           @"52": @"NO CURRENT \nACCOUNT",
                           @"53": @"NO SAVINGS \nACCOUNT",
                           @"54": @"CARD EXPIRED",
                           @"55": @"INCORRECT \nPIN",
                           @"56": @"NO CARD \nRECORD",
                           @"57": @"TRANSACTION \nNOT PERMITTED",
                           @"58": @"TRANSACTION \nNOT PERMITTED",
                           @"59": @"SUSPECTED \nFRAUD",
                           @"60": @"CONTACT \nACQUIRER",
                           @"61": @"EXCEEDS \nWITHDRAWAL LIMIT",
                           @"62": @"RESTRICTED CARD",
                           @"63": @"SECURITY \nVIOLATION",
                           @"64": @"ORIGINAL \nAMOUNT INCORRECT",
                           @"65": @"EXCEEDS WITHDRAWAL FREQ",
                           @"66": @"CALL ACQUIRER SECURITY",
                           @"67": @"HARD CAPTURE",
                           @"68": @"RESPONSE RECEIVED \nTOO LATE",
                           @"75": @"PIN TRY EXCEEDED",
                           @"77": @"BANK INTERVENE",
                           @"78": @"BANK INTERVENE",
                           @"90": @"CUT-OFF IN PROGRESS",
                           @"91": @"ISSUER OR \nSWITCH INOPERATIVE",
                           @"92": @"ROUTING ERROR",
                           @"93": @"LAW VIOLATION",
                           @"94": @"DUPLICATE TRANSACTION",
                           @"95": @"RECONCILE ERROR",
                           @"96": @"  SYSTEM MALFUNCTION",
                           @"98": @"EXCEEDS CASH LIMIT"
                        };
    
    return [dict objectForKey:value];
}


@end
