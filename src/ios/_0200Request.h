//
//  _0200Request.h
//  MyBluetoothTest
//
//  Created by Emmanuel Adeyemi on 2/9/16.
//  Copyright © 2016 Emmanuel Adeyemi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface _0200Request : NSObject


@property(getter=getPan, setter=setPan:, assign) NSString *pan;
@property(getter=getProcessingCode, setter=setProcessingCode:, assign) NSString *processingCode;
@property(getter=getTransactionAmount, setter=setTransactionAmount:, assign) NSString *transactionAmount;
@property(getter=getTransmissionDateTime, setter=setTransmissionDateTime:, assign) NSString *transmissionDateTime;
@property(getter=getStan, setter=setStan:, assign) NSString *stan;
@property(getter=getLocalTransactionTime, setter=setLocalTransactionTime:, assign) NSString *localTransactionTime;
@property(getter=getLocalTransactionDate, setter=setLocalTransactionDate:, assign) NSString *localTransactionDate;
@property(getter=getCardExpirationDate, setter=setCardExpirationDate:, assign) NSString *cardExpirationDate;
@property(getter=getMerchantType, setter=setMerchantType:, assign) NSString *merchantType;
@property(getter=getPosEntryMode, setter=setPosEntryMode:, assign) NSString *posEntryMode;
@property(getter=getCardSequenceNumber, setter=setCardSequenceNumber:, assign) NSString *cardSequenceNumber;
@property(getter=getPosConditionCode, setter=setPosConditionCode:, assign) NSString *posConditionCode;
@property(getter=getPosPinCaptureCode, setter=setPosPinCaptureCode:, assign) NSString *posPinCaptureCode;
@property(getter=getTransactionFee, setter=setTransactionFee:, assign) NSString *transactionFee;
@property(getter=getAcquiringInstIdCode, setter=setAcquiringInstIdCode:, assign) NSString *acquiringInstIdCode;
@property(getter=getTrack2Data, setter=setTrack2Data:, assign) NSString *track2Data;
@property(getter=getRetrievalReferenceNumber, setter=setRetrievalReferenceNumber:, assign) NSString *retrievalReferenceNumber;
@property(getter=getTerminalId, setter=setTerminalId:, assign) NSString *terminalId;
@property(getter=getCardAcceptorIdCode, setter=setCardAcceptorIdCode:, assign) NSString *cardAcceptorIdCode;
@property(getter=getCardAcceptorNameLocation, setter=setCardAcceptorNameLocation:, assign) NSString *cardAcceptorNameLocation;
@property(getter=getCurrencyCode, setter=setCurrencyCode:, assign) NSString *currencyCode;
@property(getter=getPinData, setter=setPinData:, assign) NSString *pinData;
@property(getter=getIccData, setter=setIccData:, assign) NSString *iccData;
@property(getter=getPosDataCode, setter=setPosDataCode:, assign) NSString *posDataCode;
@property(getter=getHashValue, setter=setHashValue:, assign) NSString *hashValue;
@property(getter=getServiceRestrictionCode, setter=setServiceRestrictionCode:, assign) NSString *serviceRestrictionCode;
@property(getter=getSecurityRelatedInformation, setter=setSecurityRelatedInformation:, assign) NSString *securityRelatedInformation;
@property(getter=getAdditionalAmounts, setter=setAdditionalAmounts:, assign) NSString *additionalAmounts;
@property(getter=getMessageReasonCode, setter=setMessageReasonCode:, assign) NSString *messageReasonCode;
@property(getter=getTransportData, setter=setTransportData:, assign) NSString *transportData;
@property(getter=getPaymentInformation, setter=setPaymentInformation:, assign) NSString *paymentInformation;
@property(getter=getSecondaryHashValue, setter=setSecondaryHashValue:, assign) NSString *secondaryHashValue;
@property(getter=getOriginalDataElements, setter=setOriginalDataElements:, assign) NSString *originalDataElements;
@property(getter=getReplacementAmount, setter=setReplacementAmount:, assign) NSString *replacementAmount;



@end
